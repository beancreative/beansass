# Bean Node-Sass

To INSTALL components simply run in root directory

	npm install
	bower install

IF 'node: command not found'

	make sure you have installed Node

IF 'bower: command not found'

	RUN
	npm install -g bower --save



RUN WITH

	node server
	Go to http://localhost:5000/ in browser to view



KEY FILES:
	
	server.js: Node server file
	/public folder: normal server stuff in here like index.html and various assets
	/public/styles folder: DO NOT CHANGE this is where SASS files get compiled to CSS. Can add aditional css file here if you'd like though
	/sass folder: location of all SASS files and get compliled on request from client if needed.  Compass and Foundation files are located here.
	/sass/main.scss: this is a test SASS file put together to show how it works. Gets called in index.html when including main.css so gets copiled into /public/styles/main.css 


Should be everything! Good Luck!

